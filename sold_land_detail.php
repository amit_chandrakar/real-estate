<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
    $propertyid = $_REQUEST['propertyid'];
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<?php include('includes/title.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>
<body class="theme-purple" onload="enable()">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Top Bar -->
<?php include('includes/top_navbar.php'); ?>

<!-- Main Content -->
<section class="home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Land Detail
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">  
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="property_list.php">Property</a></li>
                    <li class="breadcrumb-item active">Land Detail</li>
                </ul>                
            </div>
        </div>
    </div>

        <?php
        $data = "SELECT * FROM property p INNER JOIN amenities a ON a.propertyid=p.propertyid WHERE a.propertyid=".$_REQUEST['propertyid'];
        $res = mysql_query($data,$conn);
        $row=mysql_fetch_assoc($res);
        ?>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <div class="body">
                    <div id="demo2" class="carousel slide" data-ride="carousel">
                        <ul class="carousel-indicators">
                            <li data-target="#demo2" data-slide-to="0" class=""></li>
                            <li data-target="#demo2" data-slide-to="1" class="active"></li>
                            <li data-target="#demo2" data-slide-to="2" class=""></li>
                        </ul>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img style="width: 100%; height: 350px;" src="<?php if($row['photo1']==""){echo"assets/images/image-gallery/5.jpg";}else{echo"property_papers/".$propertyid."/".$row['photo1'];} ?>" class="img-fluid">
                                
                            </div>
                            <div class="carousel-item">
                                <img style="width: 100%; height: 350px;" src="<?php if($row['photo2']==""){echo"assets/images/image-gallery/5.jpg";}else{echo"property_papers/".$propertyid."/".$row['photo2'];} ?>" class="img-fluid" alt="">
                                
                            </div>
                            <div class="carousel-item">
                                <img style="width: 100%; height: 350px;" src="<?php if($row['photo3']==""){echo"assets/images/image-gallery/5.jpg";}else{echo"property_papers/".$propertyid."/".$row['photo3'];} ?>" class="img-fluid" alt="">
                                
                            </div>
                        </div>
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo2" data-slide="prev"><span class="carousel-control-prev-icon"></span></a>
                        <a class="carousel-control-next" href="#demo2" data-slide="next"><span class="carousel-control-next-icon"></span></a>
                    </div>
                    </div>
                </div>


                 <div class="card property_list">
                    <div class="body">
                        <div class="property-content">
                            <div class="detail">
                                <h5 class="text-success m-t-0 m-b-0">&#8377;<?=$row['total_amt']?></h5>
                                    <h4 class="m-t-0">
                                        <a href="#" class="col-blue-grey">
                                        <?php 
                                            if ($row['property_type']=="HOUSE" || $row['property_type']=="APARTMENT") 
                                            {
                                                echo $row['p_amount']." BHK, ".$row['land_address'];
                                            }
                                            elseif ($row['property_type']=="FORM HOUSE" || $row['property_type']=="FIELD" || $row['property_type']=="PLOT") 
                                            {
                                                echo $row['p_amount']." Acres, ".$row['land_address'];
                                            }
                                            elseif ( $row['property_type']=="OFFICE") 
                                            {
                                                echo $row['p_amount']." Rooms, ".$row['land_address'];
                                            }
                                        ?>
                                        </a>
                                    </h4>
                                <p class="text-muted"><i class="zmdi zmdi-pin m-r-5"></i><?=$row['land_address']?></p>
                                <p class="text-muted m-b-0"><?=$row['property_remark']?></p>
                            </div>
                            <div class="property-action m-t-15" style="display: none;">
                                <a href="#" title="Square Feet"><i class="zmdi zmdi-view-dashboard"></i><span>280</span></a>
                                <a href="#" title="Bedroom"><i class="zmdi zmdi-hotel"></i><span>4</span></a>
                                <a href="#" title="Parking space"><i class="zmdi zmdi-car-taxi"></i><span>2</span></a>
                                <a href="#" title="Garages"><i class="zmdi zmdi-home"></i><span> 24H</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2><strong>General</strong> Amenities<small >Description Text Here...</small></h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <ul class="list-unstyled proprerty-features">

                                    <?php if ($row['main_road']!=0) 
                                    { ?>
                                        <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Main Road</li>
                             <?php  } ?>
                                    

                                    <?php if ($row['green_zone']!=0) 
                                    { ?>
                                        <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Green Zone</li>
                             <?php  } ?>
                                    

                                    <?php if ($row['church']!=0) 
                                    { ?>
                                        <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Church</li>
                             <?php  } ?>
                                    

                                    <?php if ($row['hospital']!=0) 
                                    { ?>
                                        <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Hospital</li>
                             <?php  } ?>
                                    

                                    <?php if ($row['school']!=0) 
                                    { ?>
                                       <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near School</li> 
                             <?php  } ?>
                                    

                                    <?php if ($row['shop']!=0) 
                                    { ?>
                                        <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Shop</li>
                            <?php  } ?>
                                    

                                    <?php if ($row['estate']!=0) 
                                    { ?>
                                      <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Estate</li>  
                             <?php  } ?>
                                    
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <ul class="list-unstyled proprerty-features">


                                <?php if ($row['park']!=0) 
                                { ?>
                                    <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Parking</li>
                         <?php  } ?>


                                <?php if ($row['garden']!=0) 
                                { ?>
                                    <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Garden</li>
                         <?php  } ?>


                                <?php if ($row['pool']!=0) 
                                { ?>
                                    <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Swimming pool</li>
                         <?php  } ?>

                    
                                <?php if ($row['ac']!=0) 
                                { ?>
                                    <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Air conditioning</li>
                         <?php  } ?>
                                

                                <?php if ($row['internet']!=0) 
                                { ?>
                                    <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Internet</li>
                         <?php  } ?>
                                

                                <?php if ($row['balcony']!=0) 
                                { ?>
                                    <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Balcony</li>
                         <?php  } ?>
                                

                                <?php if ($row['terrace']!=0) 
                                { ?>
                                    <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Roof terrace</li>
                         <?php  } ?>
                                

                                <?php if ($row['tv']!=0) 
                                { ?>
                                    <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Cable TV</li>
                         <?php  } ?>
                                

                                <?php if ($row['borewell']!=0) 
                                { ?>
                                    <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Borewell</li>
                         <?php  } ?>

                            </ul>
                            </div>
                            <div class="col-sm-4">
                                <ul class="list-unstyled proprerty-features">
                                    <?php if ($row['computer']!=0) 
                                    { ?>
                                        <li><i class="zmdi zmdi-star text-warning m-r-5"></i>computer</li>
                            <?php  } ?>
                                    

                                    <?php if ($row['servent_room']!=0) 
                                    { ?>
                                        <li><i class="zmdi zmdi-star text-warning m-r-5"></i>servent_room</li>
                            <?php  } ?>
                                    

                                    <?php if ($row['pooja_room']!=0) 
                                    { ?>
                                       <li><i class="zmdi zmdi-star text-warning m-r-5"></i>pooja_room</li> 
                            <?php  } ?>
                                        
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="card" style="display: none;">
                    <div class="header">
                        <h2><strong>Location</strong> <small>Description text here...</small> </h2>
                    </div>
                    <div class="body">
                        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=svdezAlqZP2WIeKGiLW4EUnoJvnxVP7i&amp;width=100%&amp;height=400&amp;lang=tr_TR&amp;sourceType=constructor&amp;scroll=true"></script>
                    </div>
                </div>
            </div>

            <?php
            $data1 = "SELECT * FROM profile WHERE userid=".$row['buyer_id'];
            $res1 = mysql_query($data1,$conn);
            $row1=mysql_fetch_assoc($res1);
            ?>

            <div class="col-lg-4 col-md-12">
                <div class="card member-card">
                    <div class="header l-parpl">
                        <h4 class="m-t-10"><?=$row1['fname'],' ',$row1['mname'],' ',$row1['lname']?></h4>
                    </div>
                    <div class="member-img">
                        <a href="#">
                            <?php
                                $data11 = "SELECT * FROM profiledoc WHERE profid=".$row['buyer_id'];
                                $res11 = mysql_query($data11,$conn);
                                $row11=mysql_fetch_assoc($res11);
                                if ($row11['photo']=="")
                                {
                                    echo "<img src='assets/images/lg/avatar2.jpg' class='rounded-circle' alt='profile-image'>";
                                }
                                else
                                { ?>
<img style="height: 110px; width: 110px; margin-top: 15px;" class="rounded-circle" alt="profile-image" src="parties_paper/<?=$row['buyer_id']?>/<?=$row11['photo']?>" >
                              <?php  }
                                ?>
                        </a>
                    </div>
                    <div class="body">
                        <div class="col-12">
                            <p class="text-muted">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-1">
                            </div>
                            <div class="col-5">
                                <h5>
                                    <?php
                                    $data2 = "SELECT COUNT(*) as tp, SUM(total_amt) as amt FROM property WHERE buyer_id=".$row['buyer_id'];
                                    $res2 = mysql_query($data2,$conn);
                                    $row2=mysql_fetch_assoc($res2);
                                    echo $row2['tp'];
                                    ?>
                                </h5>
                                <small>Properties</small>
                            </div>
                            <div class="col-5">
                                <h5>&#8377;<?=$row2['amt']?></h5>
                                <small>Spent</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2><strong>Details</strong></h2>
                    </div>
                    <div class="body">                        
                        <div class="table-responsive">
                            <table class="table table-bordered m-b-0">
                                <tbody>
                                    <tr>
                                        <th scope="row">seller_id:</th>
                                        <td><?=$row['seller_id']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">property_type:</th>
                                        <td><?=$row['property_type']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">receipt_no:</th>
                                        <td><?=$row['receipt_no']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">receipt_amt:</th>
                                        <td><?=$row['receipt_amt']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">receipt_date:</th>
                                        <td><?=$row['receipt_date']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">sale_amt:</th>
                                        <td><?=$row['sale_amt']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">market_amt:</th>
                                        <td><?=$row['market_amt']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">stamp_amt:</th>
                                        <td><?=$row['stamp_amt']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">gramp_rate:</th>
                                        <td><?=$row['gramp_rate']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">nagern_rate:</th>
                                        <td><?=$row['nagern_rate']?></td>
                                    </tr>


                                    <tr>
                                        <th scope="row">sub_tax:</th>
                                        <td><?=$row['sub_tax']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">other_tax:</th>
                                        <td><?=$row['other_tax']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">total_amt:</th>
                                        <td><?=$row['total_amt']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">cheque_details:</th>
                                        <td><?=$row['cheque_details']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">oldowner_details:</th>
                                        <td><?=$row['oldowner_details']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">currentuse_details:</th>
                                        <td><?=$row['currentuse_details']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">any_otherdetail:</th>
                                        <td><?=$row['any_otherdetail']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">land_khasarano:</th>
                                        <td><?=$row['land_khasarano']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">land_area:</th>
                                        <td><?=$row['land_area']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">land_patwari_halkano:</th>
                                        <td><?=$row['land_patwari_halkano']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">land_plotno:</th>
                                        <td><?=$row['land_plotno']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">land_position:</th>
                                        <td><?=$row['land_position']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">land_tracedby:</th>
                                        <td><?=$row['land_tracedby']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">land_agrimentdate:</th>
                                        <td><?=$row['land_agrimentdate']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">witness1n:</th>
                                        <td><?=$row['witness1n']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">witness1add:</th>
                                        <td><?=$row['witness1add']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">witness2n:</th>
                                        <td><?=$row['witness2n']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">witness2add:</th>
                                        <td><?=$row['witness2add']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">land_otherdetail:</th>
                                        <td><?=$row['land_otherdetail']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">tax_receiptno:</th>
                                        <td><?=$row['tax_receiptno']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">tax_receiptdate:</th>
                                        <td><?=$row['tax_receiptdate']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">tax_totamt:</th>
                                        <td><?=$row['tax_totamt']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">tax_property:</th>
                                        <td><?=$row['tax_property']?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js --> 

<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script>
<?php include('includes/own.php'); ?>
</body>
</html>
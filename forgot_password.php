﻿<?php 
    if (isset($_POST['submit'])) 
    {
        $email = $_POST['email'];
        include "includes/config.php";
        $sql = "SELECT * from user WHERE uemail='$email' "; 
        $query = mysql_query($sql, $conn);
        if(mysql_num_rows($query)>1)
        { 
            $row = mysql_fetch_assoc($query);
            $userid = $row['userid'];
            $fname = $row['fname'];
            $mname = $row['mname'];
            $lname = $row['lname'];
            $subject = "Reset Password Link";

            $message = "
            <html>
            <head>
            <title>Reset Password Link</title>
            </head>
            <body>
            <p>Hello ".$fname." ".$mname." ".$lname.", Click Below link to reset your password</p>
            <table>
            <tr>
            <td>http://maierp.in/property/reset_password.php?userid=".$userid."</td>
            </tr>
            </table>
            </body>
            </html>
            ";

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: <property@example.com>';
            mail($email,$subject,$message,$headers);
        }
        else
        { 
            echo "<script>alert('Email Not Found');</script>";
            // header('Location:forgot_password.php?success=0');
        }
    }
?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <?php include('includes/title.php'); ?>
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-purple authentication sidebar-collapse">

<div class="page-header">
    <div class="page-header-image" style="background-image:url(assets/images/login.jpg)"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" method="post">
                    <div class="header">
                        <div class="logo-container">
                            <img src="assets/images/house.png" alt="">
                        </div>
                        <h5>Forgot Password?</h5>
                        <span>Enter your e-mail address below to reset your password.</span>
                    </div>
                    <div class="content">
                        <div class="input-group input-lg">
                            <input type="email" class="form-control" placeholder="Enter Email" name="email" autocomplete="off" required>
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-email"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <!-- <a href="index.php" >SUBMIT</a> -->
                        <input class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light" type="submit" name="submit" value="SUBMIT">

                        <h6 class="m-t-20"><a href="index.php" class="link">Suddenly Remember Password?</a></h6>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
</script>
</body>
</html>
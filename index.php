﻿<?php 
    session_start();
    if (isset($_SESSION['login']))
    {
        header('Location: dashboard.php');
    }
    if (isset($_POST['submit'])) 
    {
        include "includes/config.php";
        $sql = mysql_query("SELECT * from user WHERE uemail='".$_POST['username']."' AND ustatus='A' AND (utype='S' OR utype='A' OR utype='U')", $conn);
         $row=mysql_fetch_assoc($sql);
        if(mysql_num_rows($sql)==1)
        { 
            $EncPass=$row['upwd'];  
            if(($row['uemail']==$_POST['username']) AND ($EncPass==$_POST['password']) AND $row['ustatus']=='A' AND ($row['utype']=='S' OR $row['utype']=='A' OR $row['utype']=='U'))
            { 
                $_SESSION['login']=true; 
                $_SESSION['userid']=$row['userid'];
                $_SESSION['utype']=$row['utype'];
                $_SESSION['uH']=$row['uH']; 
                $_SESSION['ufullname']=$row['ufullname']; 
                $_SESSION['ustatus']=$row['ustatus'];
                $_SESSION['uedit']=$row['uedit'];
                header('Location:dashboard.php');
            }
        }
        else
        { 
            header('Location:index.php?fail=1');
        }
    }
?>


<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php include('includes/title.php'); ?>
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-purple authentication sidebar-collapse">
<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" style="background-image:url(assets/images/login.jpg)"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" method="post" action="index.php">
                    <div class="header">
                        <div class="logo-container">
                            <img src="assets/images/house.png" alt="">
                        </div>
                        <h5>Log in</h5>
                    </div>
                    <div class="content">                                                
                        <div class="input-group input-lg">
                            <input type="email" class="form-control" placeholder="Enter User Name" name="username" autofocus autocomplete="off">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                        <div class="input-group input-lg">
                            <input type="password" placeholder="Password" class="form-control" name="password" autocomplete="off">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <input type="submit" name="submit" class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light" value="SIGN IN">
                        <h6 class="m-t-20"><a href="forgot_password.php" class="link">Forgot Password?</a></h6>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>
</body>
</html>
﻿<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
    if (isset($_REQUEST['delete_userid'])) 
    {   
        $delete_userid=$_REQUEST['delete_userid'];
        $check = "UPDATE profile SET status=0 WHERE userid='$delete_userid'";
        $res = mysql_query($check, $conn);
        if ($res) 
        {
            echo '<script>alert("Party Deleted successfully!")</script>';
            echo '<script>window.location="parties.php?user_deleted=yes";</script>';
        }
    }
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<?php include('includes/title.php'); ?>
<!-- Favicon-->
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="assets/plugins/footable-bootstrap/css/footable.standalone.min.css">
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-purple" onload="enable()">

<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-left">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-arrow-left"></i> Back</a></li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="add_party.php" title="Add New Property" class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10"><i class="zmdi zmdi-plus" style="margin-top: 10px;"></i></a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Parties</li>
                </ul>
            </div>
            <div class="col-lg-12 col-md-6 col-sm-12">
                <h2>Parties
                <small class="text-muted">Welcome to Compass</small>
                </h2>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card action_bar">
                    <div class="body">
                        <form action="parties.php" method="post">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-6">
                                <label>Party Name</label>
                                <select class="form-control show-tick" name="seller">
                                    <option value="">-- Select --</option>
                                    <?php
                                       $data = "SELECT * FROM profile WHERE status=1 AND userid!=".$_SESSION['userid'];
                                      $res = mysql_query($data,$conn);
                                      while ($row=mysql_fetch_assoc($res)) 
                                      { ?>
                                      <option <?php if(isset($_REQUEST['seller'])&&$_REQUEST['seller']==$row['userid']){echo "selected";} ?> value="<?=$row['userid']?>"><?=$row['fname'],' ',$row['mname'],' ',$row['lname'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-3 col-6">
                                <label>Email</label>
                                <select class="form-control show-tick" name="email">
                                    <option value="">-- Select --</option>
                                    <?php
                                       $data = "SELECT * FROM profile WHERE status=1 AND userid!=".$_SESSION['userid'];
                                      $res = mysql_query($data,$conn);
                                      while ($row=mysql_fetch_assoc($res)) 
                                      { ?>
                                      <option <?php if(isset($_REQUEST['email'])&&$_REQUEST['email']==$row['email']){echo "selected";} ?> value="<?=$row['email']?>"><?=$row['email']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-3 col-6" style="margin-top: 28px;">
                                <div class="input-group search">
                                    <button type="submit" class="btn btn-round btn-primary waves-effect">Search</button>&nbsp;&nbsp;&nbsp;
                                <button type="Reset" class="btn btn-round btn-primary waves-effect">Reset</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>           
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-hover m-b-0 c_list">
                            <thead>
                                <tr>
                                    <th>Name</th>                                    
                                    <th data-breakpoints="xs">Phone</th>
                                    <th data-breakpoints="xs sm md">Email</th>
                                    <th data-breakpoints="xs sm md">Address</th>
                                    <th data-breakpoints="xs">Action</th>
                                </tr>
                            </thead>
                                <tbody>

                                    <?php
                                     $data = "SELECT * FROM profile WHERE status=1 AND userid!=".$_SESSION['userid'];
                                    $res = mysql_query($data,$conn);



                                    




                                    while ($row=mysql_fetch_assoc($res)) 
                                    { ?>
                                        <tr>
                                    <td>
                                        <?php
                                            $data1 = "SELECT * FROM profiledoc WHERE profid=".$row['userid'];
                                            $res1 = mysql_query($data1,$conn);
                                            $row1=mysql_fetch_assoc($res1);
                                        ?>
                                        <img style="height: 40px; width: 40px;" src="
                                        <?php if($row1['photo']!=''){ ?>parties_paper/<?=$row['userid']?>/<?=$row1['photo']?> <?php }else{ ?> assets/images/xs/avatar1.png <?php } ?>" class="rounded-circle avatar" alt="<?php echo"parties_paper/".$row['userid']."/".$row1['photo']; ?>">

                                        <span class="c_name"><?=ucwords($row['fname'])," ", ucwords($row['mname'])," ", ucwords($row['lname'])?></span>
                                    </td>
                                    <td>
                                        <a href="tel:<?=$row['contact']?>">
                                        <span class="phone"><i class="zmdi zmdi-phone m-r-10"></i><?php
                                        $phone = $row['contact'];
                                        $numbers_only = preg_replace("/[^\d]/", "", $phone);
                                        echo preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);
                                        ?></span>
                                        </a>
                                    </td>
                                    <td>
                                        <span class="email"><a href="mailto:<?=$row['email']?>"><i class="zmdi zmdi-email m-r-5"></i><?=$row['email']?></a></span>
                                    </td>
                                    <td>
                                        <address><i class="zmdi zmdi-pin"></i><?php if($row['adrscurr']==""){echo "Not Available";}else{echo $row['adrscurr'];}?></address>
                                    </td>
                                    <td>
                                        <button onclick="javascript:location.href='edit_party.php?userid=<?=$row['userid']?>'" class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round"><i class="zmdi zmdi-edit"></i></button>

                                        <!-- <button class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round"><i class="zmdi zmdi-delete"></i></button> -->

                                        <a name="a" onclick="return confirm('Are you sure?')" href="parties.php?delete_userid=<?=$row['userid']?>" class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round"><i style="margin-top: 10px;" class="zmdi zmdi-delete"></i></a>

                                        <button class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round" onclick="javascript:location.href='party_document.php?userid=<?=$row['userid']?>'"><i class="zmdi zmdi-file-plus"></i></button>
                                    </td>
                                </tr>
                                  <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
  
    </div>
</section>
<!-- Jquery Core Js -->

<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<script src="assets/bundles/footable.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/pages/tables/footable.js"></script><!-- Custom Js -->
<?php include('includes/own.php'); ?>

</body>
</html>
﻿<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
?>
<!doctype html>
<html class="no-js " lang="en">

<!-- Mirrored from thememakker.com/wrap-theme/compass/estate/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Nov 2018 09:34:36 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<?php include('includes/title.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>
<body class="theme-purple" onload="enable()">

<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Property Description
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">         
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href=""> Sales</a></li>
                    <li class="breadcrumb-item active">Property Description</li>
                </ul>                
            </div>
        </div>
    </div>
    <?php
        $data = "SELECT * FROM property WHERE propertyid=".$_REQUEST['propertyid'];
        $res = mysql_query($data,$conn);
        $row=mysql_fetch_assoc($res);
    ?>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <table width="100%" class="table table-bordered table-striped">
                            <tr>
                                <td>Seller</td>
                                <td><?=$row['seller_id']?></td>
                            </tr>
                            <tr>
                                <td>Buyer</td>
                                <td><?=$row['buyer_id']?></td>
                            </tr>
                            <tr>
                                <td>Property Type</td>
                                <td><?=$row['property_type']?></td>
                            </tr>
                            <tr>
                                <td>Age</td>
                                <td><?=$row['receipt_no']?></td>
                            </tr>
                            <tr>
                                <td>receipt_amt</td>
                                <td><?=$row['receipt_amt']?></td>
                            </tr>
                            <tr>
                                <td>receipt_date</td>
                                <td><?=$row['receipt_date']?></td>
                            </tr>
                            <tr>
                                <td> sale_amt</td>
                                <td><?=$row['sale_amt']?></td>
                            </tr>
                            <tr>
                                <td>market_amt </td>
                                <td><?=$row['market_amt']?></td>
                            </tr>
                            <tr>
                                <td>stamp_amt</td>
                                <td><?=$row['stamp_amt']?></td>
                            </tr>
                            <tr>
                                <td>gramp_rate</td>
                                <td><?=$row['gramp_rate']?></td>
                            </tr>
                            <tr>
                                <td>nagern_rate</td>
                                <td><?=$row['nagern_rate']?></td>
                            </tr>
                            <tr>
                                <td>sub_tax</td>
                                <td><?=$row['sub_tax']?></td>
                            </tr>
                            <tr>
                                <td>other_tax</td>
                                <td><?=$row['other_tax']?></td>
                            </tr>
                            <tr>
                                <td>total_amt</td>
                                <td><?=$row['total_amt']?></td>
                            </tr>
                            <tr>
                                <td>cheque_details</td>
                                <td><?=$row['cheque_details']?></td>
                            </tr>
                            <tr>
                                <td>oldowner_details</td>
                                <td><?=$row['oldowner_details']?></td>
                            </tr>
                            <tr>
                                <td>currentuse_details</td>
                                <td><?=$row['currentuse_details']?></td>
                            </tr>
                            <tr>
                                <td>any_otherdetail</td>
                                <td><?=$row['any_otherdetail']?></td>
                            </tr>
                            <tr>
                                <td>land_khasarano</td>
                                <td><?=$row['land_khasarano']?></td>
                            </tr>
                            <tr>
                                <td>land_area</td>
                                <td><?=$row['land_area']?></td>
                            </tr>
                            <tr>
                                <td>land_patwari_halkano</td>
                                <td><?=$row['land_patwari_halkano']?></td>
                            </tr>
                            <tr>
                                <td>land_address</td>
                                <td><?=$row['land_address']?></td>
                            </tr>
                            <tr>
                                <td>land_plotno</td>
                                <td><?=$row['land_plotno']?></td>
                            </tr>
                            <tr>
                                <td>land_position</td>
                                <td><?=$row['land_position']?></td>
                            </tr>
                            <tr>
                                <td>land_tracedby</td>
                                <td><?=$row['land_tracedby']?></td>
                            </tr>
                            <tr>
                                <td>land_agrimentdate</td>
                                <td><?=$row['land_agrimentdate']?></td>
                            </tr>
                            <tr>
                                <td>witness1n</td>
                                <td><?=$row['witness1n']?></td>
                            </tr>
                            <tr>
                                <td>witness1add</td>
                                <td><?=$row['witness1add']?></td>
                            </tr>
                            <tr>
                                <td>witness2n</td>
                                <td><?=$row['witness2n']?></td>
                            </tr>
                            <tr>
                                <td>witness2add</td>
                                <td><?=$row['witness2add']?></td>
                            </tr>
                            <tr>
                                <td>land_otherdetail</td>
                                <td><?=$row['land_otherdetail']?></td>
                            </tr>
                            <tr>
                                <td>tax_receiptno</td>
                                <td><?=$row['tax_receiptno']?></td>
                            </tr>
                            <tr>
                                <td>tax_receiptdate</td>
                                <td><?=$row['tax_receiptdate']?></td>
                            </tr>
                            <tr>
                                <td>tax_totamt</td>
                                <td><?=$row['tax_totamt']?></td>
                            </tr>
                            <tr>
                                <td>tax_property</td>
                                <td><?=$row['tax_property']?></td>
                            </tr>
                            <tr>
                                <td>tax_fire</td>
                                <td><?=$row['tax_fire']?></td>
                            </tr>
                            <tr>
                                <td>tax_clean</td>
                                <td><?=$row['tax_clean']?></td>
                            </tr>
                            <tr>
                                <td>tax_water</td>
                                <td><?=$row['tax_water']?></td>
                            </tr>
                            <tr>
                                <td>tax_normalburnt</td>
                                <td><?=$row['tax_normalburnt']?></td>
                            </tr>
                        </table>
                        <center> 
                            <button class="btn btn-warning btn-round btn-simple" onclick="javascript:location.href='sale.php'">Cancel</button> 
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js --> 

<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/mainscripts.bundle.js"></script>
<?php include('includes/own.php'); ?>
</body>
</html>
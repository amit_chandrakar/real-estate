﻿<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
    error_reporting(0);
    if (isset($_POST['add_new'])) 
    {   
        $check = "INSERT INTO notification (title, description, n_date) VALUES('".$_POST['title']."', '".$_POST['description']."', '".$_POST['date']."')   ";
        $res = mysql_query($check, $conn);
        if ($res) 
        {
            echo '<script>alert("Notification added successfully!")</script>';
            echo '<script>window.location="notification.php";</script>';
        }
    }
    elseif (isset($_REQUEST['delete_not'])) 
    {   
        $check = "DELETE FROM notification WHERE id=".$_REQUEST['delete_not'];
        $res = mysql_query($check, $conn);
        if ($res) 
        {
            echo '<script>alert("Notification Deleted successfully!")</script>';
            echo '<script>window.location="notification.php";</script>';
        }
    }
    elseif (isset($_POST['edit_not'])) 
    {   
        $check = "UPDATE notification SET title='".$_POST['title']."', description='".$_POST['description']."', n_date='".$_POST['date']."' WHERE id=".$_REQUEST['note_id'];
        $res = mysql_query($check, $conn);
        if ($res) 
        {
            echo '<script>alert("Notification Updated successfully!")</script>';
            echo '<script>window.location="notification.php";</script>';
        }
        // echo "<script>alert('dskf');</script>";
    }
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<?php include('includes/title.php'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<!-- Favicon-->
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-purple" onload="enable()">

<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-left">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-arrow-left"></i> Back</a></li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <!-- Trigger the modal with a button -->
                <button type="button" class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10" data-toggle="modal" data-target="#myModal"><i class="zmdi zmdi-plus"></i></button>


                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Notification</li>
                </ul>
            </div>
            <div class="col-lg-12 col-md-6 col-sm-12">
                <h2>Notifications
                <small class="text-muted">Welcome to Compass</small>
                </h2>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-hover table-striped table-bordered" id="example2" style="width:100%">
                            <thead>
                                <tr>
                                    <th>S. No.</th>                                    
                                    <th data-breakpoints="xs">Title</th>
                                    <th data-breakpoints="xs sm md">Description</th>
                                    <th data-breakpoints="xs sm md">Notification Date</th>
                                    <th data-breakpoints="xs">Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>S. No.</th>                                    
                                    <th data-breakpoints="xs">Title</th>
                                    <th data-breakpoints="xs sm md">Description</th>
                                    <th data-breakpoints="xs sm md">Notification Date</th>
                                    <th data-breakpoints="xs">Action</th>
                                </tr>
                            </tfoot>
                                <tbody>

                                    <?php
                                     $data = "SELECT * FROM notification ORDER BY id DESC";
                                    $res = mysql_query($data,$conn);
                                    while ($row=mysql_fetch_assoc($res)) 
                                    { ?>
                                        <tr>
                                    <td><?=$row['id']?></td>
                                    <td><?=$row['title']?></td>
                                    <td><?=$row['description']?></td>
                                    <td><?=date('d-m-Y', strtotime($row['n_date']))?></td>
                                    <td>
                                        <button onclick="javascript:location.href='notification.php?note_id=<?=$row['id']?>'" class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round"><i class="zmdi zmdi-edit"></i></button>

                                        <!-- <button class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round"><i class="zmdi zmdi-delete"></i></button> -->

                                        <a name="a" onclick="return confirm('Are you sure?')" href="notification.php?delete_not=<?=$row['id']?>" class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round"><i style="margin-top: 10px;" class="zmdi zmdi-delete"></i></a>

                                    </td>
                                </tr>
                                  <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
  
    </div>
</section>
<!-- Jquery Core Js -->

<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<script src="assets/bundles/footable.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script>
<?php include('includes/own.php'); ?>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example2').DataTable();
} );
</script>
</body>
</html>


<button id="update" type="button" data-toggle="modal" data-target="#not">UBAKJ</button>
<!-- Modal -->
<?php
    if (isset($_REQUEST['note_id'])) 
    {
        $select_not = "SELECT * FROM notification WHERE id=".$_REQUEST['note_id'];
        $res_not = mysql_query($select_not, $conn);
        $rr = mysql_fetch_assoc($res_not);
    }
?>
<div id="not" class="modal fade" role="dialog">
    <form method="post">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Notification</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-4">Notification Date</div>
            <div class="col-md-8"><input value="<?=$rr['n_date']?>" class="form-control" type="date" name="date" placeholder="Enter date here"><br></div>
            <div class="col-md-4">Title</div>
            <div class="col-md-8"><input value="<?=$rr['title']?>" class="form-control" type="text" name="title" placeholder="Enter title here"><br></div>
            <div class="col-md-4">Description</div>
            <div class="col-md-8"><textarea  class="form-control" name="description" placeholder="Enter description here"><?=$rr['description']?></textarea></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="edit_not" class="btn btn-default">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
  </form>
</div>
<!-- End of Modal -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <form method="post" action="#">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Notification</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-4">Notification Date</div>
            <div class="col-md-8"><input class="form-control" type="date" name="date" placeholder="Enter date here"><br></div>
            <div class="col-md-4">Title</div>
            <div class="col-md-8"><input class="form-control" type="text" name="title" placeholder="Enter title here"><br></div>
            <div class="col-md-4">Description</div>
            <div class="col-md-8"><textarea class="form-control" name="description" placeholder="Enter description here"></textarea></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="add_new" class="btn btn-default">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
  </form>
</div>
<!-- End of Modal -->

<?php
    if (isset($_REQUEST['note_id'])) 
    { ?>
        <script>document.getElementById('update').click();</script>
    <?php } ?>




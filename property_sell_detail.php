﻿<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
    error_reporting(0);
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<?php include('includes/title.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<style type="text/css">
    label
    {
        margin-left: 15px;
    }
</style>
</head>
<body class="theme-purple" onload="enable()">

<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-left">
                    <li class="breadcrumb-item"><a href="sale.php"><i class="zmdi zmdi-arrow-left"></i> Back</a></li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="sale.php"><i class="zmdi zmdi-home"></i> Property for Sale</a></li>
                    <li class="breadcrumb-item active">Property Sale Information</li>
                </ul>                
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Property Sale Information
                <small class="text-muted">Welcome to Compass</small>
                </h2>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">


                <div class="card action_bar">
                    <?php
                        if (isset($_REQUEST['propertyid'])) 
                        {
                            $data = "SELECT * FROM property WHERE propertyid=".$_REQUEST['propertyid'];
                            $res = mysql_query($data,$conn);
                            $row =mysql_fetch_assoc($res);
                        }
                    ?>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-6">
                                <h6>Owner : <?php
                                    $data1 = "SELECT * FROM profile WHERE userid=".$row['buyer_id'];
                                    $res1 = mysql_query($data1,$conn);
                                    $row1 =mysql_fetch_assoc($res1);
                                    echo $row1['fname']," ",$row1['lname'];
                                ?>


                                 </h6>  
                            </div>
                            <div class="col-lg-3 col-md-3 col-6">
                                <h6>Property Type : <?=$row['property_type']?></h6>  
                            </div>
                            
                             <div class="col-lg-3 col-md-3 col-6">
                                <h6>Available Area :
                                    <?php
                                        if ($row['property_type']=="APARTMENT") 
                                        {
                                            echo $row['a1bhk']+$row['a2bhk']+$row['a3bhk']+$row['a4bhk'];
                                        }
                                        else
                                        {
                                            echo $row['p_amount'];
                                        }
                                    ?>
                                </h6>  
                            </div><br><br>
                            <div class="col-lg-3 col-md-3 col-6">
                                <h6>Land Khasra No : <?=$row['land_khasarano']?></h6>  
                            </div>
                            <div class="col-lg-3 col-md-3 col-6">
                                <h6>Land Halka No : <?=$row['land_patwari_halkano']?></h6>  
                            </div>
                            <div class="col-lg-3 col-md-3 col-6">
                                <h6>Land Plot No : <?=$row['land_plotno']?></h6>  
                            </div>
                            <div class="col-lg-3 col-md-3 col-6">
                                <h6>Agreement Date : <?=$row['land_agrimentdate']?></h6>  
                            </div>
                            <br><br>
                            <div class="col-lg-4 col-md-3 col-6">
                                <h6>Address : <?=$row['land_address']?></h6>  
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card" id="property_list">
                    <div class="header">
                        <h2><strong>All Property</strong> List<small>Description text here...</small> </h2>
                    </div>
                    <div class="body">
                        <div class="row">
                           <div class="col-sm-12">
                            <table class="table table-hover table-striped table-bordered" id="example2" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Owner</th>                                    
                                    <th>Rent By</th>                                    
                                    <th data-breakpoints="xs sm md">Property Type</th>
                                    <th data-breakpoints="xs sm md">Location</th>
                                    <th data-breakpoints="xs sm md">Action</th>
                                </tr>
                            </thead>
                                <tbody>

                                    <?php
                                    $data = "SELECT * FROM sale WHERE propertyid=".$_REQUEST['propertyid'];
                                    $res = mysql_query($data,$conn);
                                    while ($row=mysql_fetch_assoc($res)) 
                                    { ?>
                                        <tr>
                                    <td>
                                        <span class="c_name">
                                            <?php
                                                $d = "SELECT * FROM profile WHERE userid=".$row['seller_id'];
                                                $r = mysql_query($d,$conn);
                                                $rr=mysql_fetch_assoc($r);
                                                echo ucwords($rr['fname'])." ",ucwords($rr['lname']); 
                                            ?>
                                                
                                        </span>
                                    </td>
                                    <td>
                                        <span class="phone">
                                            <?php
                                                $d = "SELECT * FROM profile WHERE userid=".$row['buyer_id'];
                                                $r = mysql_query($d,$conn);
                                                $rr=mysql_fetch_assoc($r);
                                                echo ucwords($rr['fname'])." ",ucwords($rr['lname']); 
                                            ?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="email"><?=$row['property_type']?></span>
                                    </td>
                                    <td>
                                        <span class="email"><?=$row['land_address']?></span>
                                    </td>
                                    <td>
                                        <span> 
                                            <a href="property_sell_detail.php?propertyid=<?=$_REQUEST['propertyid']?>&saleid=<?=$row['saleid']?>" class="btn btn-warning btn-xs"><i class="zmdi zmdi-edit"></i></a><!-- edit -->
                                        </span>

                                    </td>
                                </tr>
                                  <?php } ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Owner</th>                                    
                                    <th>Rent By</th>                                    
                                    <th data-breakpoints="xs sm md">Property Type</th>
                                    <th data-breakpoints="xs sm md">Location</th>
                                    <th data-breakpoints="xs sm md">Action</th>
                                </tr>
                            </tfoot>
                        </table>
                            </div> 
                        </div>
                    </div>
                </div>




            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example2').DataTable();
} );
</script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example3').DataTable();
} );
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<?php include('includes/own.php'); ?>
</body>
</html>



















<!-- Modal for view/update rent detail -->
<?php
    if (isset($_REQUEST['saleid'])) 
    {
        $data1 = "SELECT * FROM sale WHERE saleid=".$_REQUEST['saleid'];
        $res1 = mysql_query($data1,$conn);
        $row1 =mysql_fetch_assoc($res1);
    }
?>
<button style="display: none;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#update_rentid_modal" id="btn_rent_detail"></button>

<div class="modal fade" id="update_rentid_modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sold Property Detail</h4>
            </div>
            <div class="modal-body">
                <form action="property_rent_detail.php" method="post">
                <div class="row clearfix">

                    <div class="col-sm-12">
                        <label><h6>Basic Detail</h6></label>
                        <hr>
                    </div>
                            <input type="hidden" name="propertyid" value="<?=$row1['propertyid']?>">
                            <div class="col-sm-4">
                                <label>Owner</label>
                                <div class="form-group" style="margin-top: -18px">
                                    <select class="form-control show-tick" name="owner2" id="owner2" disabled>
                                        <?php
                                           $data = "SELECT * FROM sale s INNER JOIN profile p ON p.userid=s.seller_id WHERE saleid=".$_REQUEST['saleid'];
                                          $res = mysql_query($data,$conn);
                                          while ($row=mysql_fetch_assoc($res)) 
                                          { ?>
                                          <option value="<?=$row['owner']?>"><?=$row['fname']," ",$row['lname']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label>Buyer Name</label>
                                <div class="form-group" style="margin-top: -18px">
                                    <select class="form-control show-tick" name="rent_by2" id="rent_by2" disabled>
                                        <?php
                                           $data = "SELECT * FROM sale s INNER JOIN profile p ON p.userid=s.buyer_id WHERE saleid=".$_REQUEST['saleid'];
                                          $res = mysql_query($data,$conn);
                                          while ($row=mysql_fetch_assoc($res)) 
                                          { ?>
                                          <option><?=$row['fname']," ",$row['lname']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label>Property Type</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="property_type" id="property_type"value="<?=$row1['property_type']?>" readonly>
                                </div>
                            </div>

                            <?php
                                if ($row1['property_type']!="APARTMENT") 
                                {?>
                                    <div class="col-sm-3" id="div_area2">
                                        <label>Total area</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Receipt No" name="land_area2" id="land_area2" max="100" maxlength="3" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" value="<?=$row1['p_amount']?>">
                                            <span class="input-group-addon" id="pt2" name="pt2"></span>
                                        </div>
                                    </div>
                               <?php }
                            ?>




                            <?php
                                if ($row1['property_type']=="APARTMENT") 
                                {?>
                                    <div class="col-sm-12"  id="apart2">
                                        <div style="padding-bottom: 20px;">
                                        <label><b class="h6">Enter Total area Detail</b></label>
                                        <hr>
                                        <div class="row">
                                        <div class="col-sm-3"> <label>No of 1 BHK </label>  <input type="number" class="form-control" placeholder="Receipt No" name="1bhk2" id="1bhk2" value="<?=$row1['1bhk']?>" disabled></div>
                                        <div class="col-sm-3"> <label>No of 2 BHK  </label><input type="number" class="form-control" placeholder="Receipt No" name="2bhk2" id="2bhk2" value="<?=$row1['2bhk']?>" disabled></div>
                                        <div class="col-sm-3"> <label>No of 3 BHK </label> <input type="number" class="form-control" placeholder="Receipt No" name="3bhk2" id="3bhk2" value="<?=$row1['3bhk']?>" disabled></div>
                                        <div class="col-sm-3"> <label>No of 4 BHK  </label><input type="number" class="form-control" placeholder="Receipt No" name="4bhk2" id="4bhk2" value="<?=$row1['4bhk']?>" disabled></div>
                                        </div>
                                        </div>
                                    </div>
                               <?php }
                            ?>
                        <br><br>
                        </div>



              
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <label><b class="h6"> Other Detail </b></label>
                                <hr>
                            </div>

                            
                            <div class="col-sm-3">
                                <label>Receipt No</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Gramp Rate" name="receipt_no2" id="receipt_no2" value="<?=$row1['receipt_no']?>" disabled>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label>Receipt Amount</label>
                                <div class="form-group">
                                    <input type="number" class="form-control" placeholder="Nagern Rate" name="receipt_amt2" id="receipt_amt2" value="<?=$row1['receipt_amt']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Receipt Date</label>
                                <div class="form-group">
                                    <input type="date" class="form-control" placeholder="Sub-Tax" name="receipt_date2" id="receipt_date2" value="<?=$row1['receipt_date']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label> Stamp Amount</label>
                                <div class="form-group">
                                    <input type="number" class="form-control" placeholder="Other Tax" name="stamp_amt2" id="stamp_amt2" value="<?=$row1['stamp_amt']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Other Tax</label>
                                <div class="form-group">
                                    <input type="number" class="form-control" placeholder="Total Amount" name="other_tax2" id="other_tax2" value="<?=$row1['other_tax']?>" disabled>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label>Cheque Details</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Old Owner Details" name="cheque_details2" id="cheque_details2" style="text-transform: uppercase" value="<?=$row1['cheque_details']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Current Use Details</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Current Use Details" name="currentuse_details2" id="currentuse_details2" style="text-transform: uppercase" value="<?=$row1['currentuse_details']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Any Other Detail</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Any Other Detail" name="any_otherdetail2" id="any_otherdetail2" style="text-transform: uppercase" value="<?=$row1['any_otherdetail']?>" disabled>
                                </div>
                            </div>


                            <div class="col-sm-3">
                                <label>Land Khasara No  </label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Land Khasara No" name="land_khasarano2" id="land_khasarano2" style="text-transform: uppercase" value="<?=$row1['land_khasarano']?>" disabled>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label>Land Patwari No</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Land Patwari Halka No" name="land_patwari_halkano2" id="land_patwari_halkano2" style="text-transform: uppercase" value="<?=$row1['land_patwari_halkano']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Land Address</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Land Address"  name="land_address2" id="land_address2" style="text-transform: uppercase" value="<?=$row1['land_address']?>" disabled>
                                </div>
                            </div>



                            <div class="col-sm-3">
                                <label>Land Plot No</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Land Plot No" name="land_plotno2" id="land_plotno2" style="text-transform: uppercase" value="<?=$row1['land_plotno']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Land Position</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Land Position" name="land_position2" id="land_position2" style="text-transform: uppercase" value="<?=$row1['land_position']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Land Traced By</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Land Traced By" name="land_tracedby2" id="land_tracedby2" style="text-transform: uppercase" value="<?=$row1['land_tracedby']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Land Agriment Date</label>
                                <div class="form-group">
                                    <input type="date" class="form-control" placeholder="Land Agriment Date" name="land_agrimentdate2" id="land_agrimentdate2" style="width: 100%" value="<?=$row1['land_agrimentdate']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Witness 1</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Witness 1" name="witness1n2" id="witness1n2" style="text-transform: uppercase" value="<?=$row1['witness1n']?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Witness 1 Address</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Witness 1 Address" name="witness1add2" id="witness1add2" style="text-transform: uppercase" value="<?=$row1['witness1add']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Witness 2</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Witness 2" name="witness2n2" id="witness2n2" style="text-transform: uppercase" value="<?=$row1['witness2n']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Witness 2 Address</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Witness 2 Address" name="witness2add2" id="witness2add2" style="text-transform: uppercase" value="<?=$row1['witness2add']?>" disabled>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>Land Other Details</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Land Other Details" name="land_otherdetail2" id="land_otherdetail2" style="text-transform: uppercase" value="<?=$row1['land_otherdetail']?>" disabled>
                                </div>
                            </div>
                        </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <button type="button" class="btn btn-default btn-round btn-simple" data-dismiss="modal">Close</button>
                        </center>
                    </div>
                </div>
                
            </div>
            </form>
        </div>
    </div>
</div>
</div>

<?php
    if (isset($_REQUEST['saleid']) && $_REQUEST['saleid']!="") 
    { ?>
        <script type="text/javascript">
            document.getElementById('btn_rent_detail').click();
        </script>
 <?php }
?> 
<!-- Modal for view/update rent detail -->


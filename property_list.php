<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
    error_reporting(0);
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<?php include('includes/title.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
<style type="text/css">
    label
    {
        margin-left: 20px;
    }
</style>
</head>
<body class="theme-purple" onload="enable()">

<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-left">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-arrow-left"></i> Back</a></li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="add_property.php" title="Add New Property" class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10"><i class="zmdi zmdi-plus" style="margin-top: 10px;"></i></a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Property</a></li>
                    <li class="breadcrumb-item active">List</li>
                </ul>                
            </div>
            <div class="col-lg-12 col-md-6 col-sm-12">
                <h2>Property List
                <small class="text-muted">Welcome to Compass</small>
                </h2>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="header">
                        <h2>Search</h2>
                    </div> -->
                    <div class="body">
                        <form action="property_list.php">
                        <div class="row clearfix">
                            <div class="col-sm-3 col-xs-12 col-lg-3" >
                                <label>Seller</label>
                                <select class="form-control show-tick" name="seller">
                                        <option value="">-- Select --</option>
                                        <?php
                                           $data = "SELECT * FROM profile WHERE status=1 AND userid!=".$_SESSION['userid'];
                                          $res = mysql_query($data,$conn);
                                          while ($row=mysql_fetch_assoc($res)) 
                                          { ?>
                                          <option <?php if(isset($_REQUEST['seller'])&&$_REQUEST['seller']==$row['userid']){echo "selected";} ?> value="<?=$row['userid']?>"><?=$row['fname']?></option>
                                        <?php } ?>
                                    </select>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <label>Buyer</label>
                                <select class="form-control show-tick" name="seller">
                                    <option value="">-- Select --</option>
                                    <?php
                                       $data = "SELECT * FROM profile WHERE status=1 AND userid!=".$_SESSION['userid'];
                                      $res = mysql_query($data,$conn);
                                      while ($row=mysql_fetch_assoc($res)) 
                                      { ?>
                                      <option <?php if(isset($_REQUEST['seller'])&&$_REQUEST['seller']==$row['userid']){echo "selected";} ?> value="<?=$row['userid']?>"><?=$row['fname']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <label>Type</label>
                                <select  class="form-control show-tick" name="property_type" id="property_type">
                                    <option value="">-- Select --</option>
                                    <option value="HOUSE">HOUSE</option>
                                    <option value="APARTMENT">APARTMENT</option>
                                    <option value="FORM HOUSE">FORM HOUSE</option>
                                    <option value="FIELD">FIELD</option>
                                    <option value="OFFICE">OFFICE</option>
                                    <option value="PLOT">PLOT</option>
                                    <option value="OTHER">OTHER</option>
                                </select>
                            </div>
                            <div class="col-sm-3" style="margin-top: 28px;">
                                <button type="submit" class="btn btn-round btn-primary waves-effect">Search</button>
                                <button type="Reset" class="btn btn-round btn-primary waves-effect">Reset</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">


            <?php 
                if (isset($_REQUEST['type']) && $_REQUEST['type']!="" && isset($_REQUEST['seller']) && $_REQUEST['seller']!="" && isset($_REQUEST['buyer']) && $_REQUEST['buyer']!="") 
                {
                    $data = "SELECT * FROM property p INNER JOIN amenities a ON p.propertyid=a.propertyid WHERE p.sold!=1 AND p.approve='Y' AND seller_id='".$_REQUEST['seller']."' AND buyer_name='".$_REQUEST['buyer']."' AND property_type='".strtoupper($_REQUEST['type'])."'  ";
                }
                elseif (isset($_REQUEST['seller']) && $_REQUEST['seller']!="" && isset($_REQUEST['buyer']) && $_REQUEST['buyer']!="") 
                {
                    $data = "SELECT * FROM property p INNER JOIN amenities a ON p.propertyid=a.propertyid WHERE p.sold!=1 AND p.approve='Y' AND seller_id='".$_REQUEST['seller']."' AND buyer_name='".$_REQUEST['buyer']."' ";
                }
                elseif (isset($_REQUEST['type']) && $_REQUEST['type']!="" && isset($_REQUEST['seller']) && $_REQUEST['seller']!="") 
                {
                    $data = "SELECT * FROM property p INNER JOIN amenities a ON p.propertyid=a.propertyid WHERE p.sold!=1 AND p.approve='Y' AND seller_id='".$_REQUEST['seller']."' AND property_type='".strtoupper($_REQUEST['type'])."'  ";
                }
                elseif (isset($_REQUEST['type']) && $_REQUEST['type']!="" && isset($_REQUEST['buyer']) && $_REQUEST['buyer']!="") 
                {
                    $data = "SELECT * FROM property p INNER JOIN amenities a ON p.propertyid=a.propertyid WHERE p.sold!=1 AND p.approve='Y' AND buyer_name='".$_REQUEST['buyer']."' AND property_type='".strtoupper($_REQUEST['type'])."'  ";
                }
                elseif (isset($_REQUEST['seller']) && $_REQUEST['seller']!="") 
                {
                    $data = "SELECT * FROM property p INNER JOIN amenities a ON p.propertyid=a.propertyid WHERE p.sold!=1 AND p.approve='Y' AND seller_id='".$_REQUEST['seller']."'  ";
                }
                elseif (isset($_REQUEST['buyer']) && $_REQUEST['buyer']!="") 
                {
                    $data = "SELECT * FROM property p INNER JOIN amenities a ON p.propertyid=a.propertyid WHERE p.sold!=1 AND p.approve='Y' AND buyer_name='".$_REQUEST['buyer']."'  ";
                }
                elseif (isset($_REQUEST['type']) && $_REQUEST['type']!="") 
                {
                    $data = "SELECT * FROM property p INNER JOIN amenities a ON p.propertyid=a.propertyid WHERE p.sold!=1 AND p.approve='Y' AND property_type='".strtoupper($_REQUEST['type'])."'  ";
                }

                else 
                {
                    $data = "SELECT * FROM property p INNER JOIN amenities a ON p.propertyid=a.propertyid WHERE p.sold!=1 AND p.approve='Y'";
                }
                $res = mysql_query($data,$conn);
                if (mysql_num_rows($res)>0) 
                {
                while ($row=mysql_fetch_assoc($res)) 
                { ?>

                    <div class="card property_list">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="property_image">
                                    <?php
                                     
                                        if ($row['photo1']!="") 
                                        { ?>
                                            <a href="land_detail.php?propertyid=<?=$row['propertyid']?>"><img class="img-thumbnail img-fluid" style="max-height: 200px;max-width: 400px;width: 400px;" src="property_papers/<?=$row['propertyid'],"/",$row['photo1']?>" ></a>
                                     <?php   }
                                        else
                                        { ?>
                                            <a href="land_detail.php?propertyid=<?=$row['propertyid']?>"><img class="img-thumbnail img-fluid" src="assets/images/image-gallery/7.jpg"></a>
                                    <?php } 
                                    
                                    ?>
                                    <span class="badge badge-danger"><?=$row['property_type']?></span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="property-content">
                                    <div class="detail">
                                    	<?php ?>
                                        <h5 class="text-success m-t-0 m-b-0">&#x20b9; <?=$row['receipt_amt']?></h5>
                                        <h4 class="m-t-0">
                                         <a href="#" class="col-blue-grey">
                                           <!-- 4BHK Alexander Court,New York  -->
                                           <?php
                                                if($row['property_type']!="APARTMENT")
                                                {
                                                    echo $row['p_amount']." BHK";
                                                }
                                                elseif($row['property_type']=="APARTMENT")
                                                {
                                                    echo "1BHK : <b>".$row['1bhk']."</b>, 2BHK : <b>".$row['2bhk']."</b>, 3BHK : <b>".$row['3bhk']."</b>, 4BHK : <b>".$row['4bhk'];
                                                }
                                                
                                           ?>
                                         </a>
                                        </h4>
                                          

                                        <p class="text-muted"><i class="zmdi zmdi-pin m-r-5"></i>
                                            <?php if($row['land_address']=="")
                                            {
                                             echo "Not Available";
                                            }
                                            else{echo $row['land_address'];}?>
                                        </p>
                                        <p class="text-muted m-b-0"><?php if($row['land_remark']=="")
                                         {
                                          echo "Remark : Property Remark Not Available";
                                         }
                                         else{echo "<b>Remark : </b>".$row['land_remark'];}?></p>
                                         
                                    </div>
                                    <div class="property-action m-t-15">
                                        <!-- <a href="#" title="Square Feet"><i class="zmdi zmdi-view-dashboard"></i><span>280 BHK</span></a>
                                        
                                        <a href="#" title="Square Feet"><i class="zmdi zmdi-view-dashboard"></i><span>280 BHK</span></a>

                                        <a href="#" title="Square Feet"><i class="zmdi zmdi-view-dashboard"></i><span>280 BHK</span></a>

                                        <a href="#" title="Square Feet"><i class="zmdi zmdi-view-dashboard"></i><span>280 BHK</span></a> -->
                                <a href="property_doc.php?propertyid=<?=$row['propertyid']?>"><span class="badge badge-default">Property Documents</span></a>
                                <a href="edit_property.php?propertyid=<?=$row['propertyid']?>"><span class="badge badge-default">Edit This Property</span></a>
                                    </div>
                                     
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

          <?php } }
            else
            { ?>
                <div class="card property_list">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-12 col-md-6">
                                <div class="property-content">
                                    <div class="detail">
                                       <center><h4 class="m-t-0"><a href="#" class="col-blue-grey">Property Not Available</a></h4></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          <?php  }
           ?>

            </div>

        </div>
    </div>
</section>
<!-- Jquery Core Js --> 

<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script>
<?php include('includes/own.php'); ?>
</body>
</html>
<?php
session_start();
require_once '../includes/check_user.php';
require_once '../includes/config.php';
$select_property = "SELECT * FROM  property_rent pr INNER JOIN rent_pay rp ON pr.rentid=rp.rentid INNER JOIN profile p ON p.userid=rp.pay_by WHERE rp.payid=".$_REQUEST['payid'];
$select_property_res = mysql_query($select_property, $conn);
$pa= mysql_fetch_assoc($select_property_res);

$data1 = "SELECT * FROM profile WHERE userid=".$pa['pay_to'];
$res1 = mysql_query($data1,$conn);
$row1 =mysql_fetch_assoc($res1);


error_reporting(E_ERROR | E_PARSE);
require_once 'autoload.inc.php';
use Dompdf\Dompdf;
$dompdf = new Dompdf();
// $html = file_get_contents("print.php");
$dompdf->loadHtml("
<!doctype html>
<html>
<head><title>Rent Receipt</title></head>
<body >
    <div class='container-fluid' style='border: 1px solid black'>
    	<div style='padding: 50px'>
        <div class='row'>
            <div class='col-lg-12'>
                <center><h2 class='text-center'>RENT RECEIPT</h2></center>
            </div>
        </div>

        <div class='row'>
        	<table width='100%'>
        		<tr>
        			<td width='75%'><b>Receipt No :</b> PROP".$_REQUEST['payid']."</td>
        			<td width='23%' style='text-align: center;'><b>Date : </b>".date('d-m-Y', strtotime($pa['paydate']))."</td>
        		</tr>
        		<tr>
        			<td><br></td>
        		</tr>
        		<tr>
        			<td colspan='2'>Received sum of INR <b>RS. ".$pa['pay_amt']."</b> from <b>".ucwords($pa['fname'])." ".ucwords($pa['mname'])." ".ucwords($pa['lname'])."</b> towards the rent of property located at <b>".ucwords(strtolower($pa['land_address']))."</b> for the period of ".$pa['pay_period']." Months and ".$pa['pay_days']." Days. </td>
        		</tr>
        		<tr>
        			<td><br></td>
        		</tr>
        		<tr>
        			<td>".ucwords($row1['fname'])." ".ucwords($row1['lname'])." (Received By)</td>
        		</tr>
        	</table>
        </div>
        </div>
    </div>
</body>
</html>"

);
$dompdf->setPaper('A5', 'landscape');
$dompdf->render();
$dompdf->stream("Rent_Receipt".$pa['paydate'],array("Attachment"=>0));







?>

<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
    error_reporting(0);
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<?php include('includes/title.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
</head>

<body class="theme-purple" onload="enable()">

<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>



<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-left">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-arrow-left"></i> Back</a></li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <a href="purchase_new.php" title="Add New Property" class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10"><i class="zmdi zmdi-plus" style="margin-top: 10px;"></i></a>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Purchase</li>
                </ul>
            </div>
            <div class="col-lg-12 col-md-6 col-sm-12">
                <h2>Purchase
                <small class="text-muted">Welcome to Compass</small>
                </h2>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <form action="property_list.php">
                        <div class="row clearfix">
                            <div class="col-sm-3 col-xs-12 col-lg-3" >
                                <label>Seller</label>
                                <select class="form-control show-tick" name="seller">
                                        <option value="">-- Select --</option>
                                        <?php
                                           $data = "SELECT * FROM profile WHERE status=1 AND userid!=".$_SESSION['userid'];
                                          $res = mysql_query($data,$conn);
                                          while ($row=mysql_fetch_assoc($res)) 
                                          { ?>
                                          <option <?php if(isset($_REQUEST['seller'])&&$_REQUEST['seller']==$row['userid']){echo "selected";} ?> value="<?=$row['userid']?>"><?=$row['fname']?></option>
                                        <?php } ?>
                                    </select>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <label>Buyer</label>
                                <select class="form-control show-tick" name="seller">
                                    <option value="">-- Select --</option>
                                    <?php
                                       $data = "SELECT * FROM profile WHERE status=1 AND userid!=".$_SESSION['userid'];
                                      $res = mysql_query($data,$conn);
                                      while ($row=mysql_fetch_assoc($res)) 
                                      { ?>
                                      <option <?php if(isset($_REQUEST['seller'])&&$_REQUEST['seller']==$row['userid']){echo "selected";} ?> value="<?=$row['userid']?>"><?=$row['fname']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <label>Type</label>
                                <select  class="form-control show-tick" name="property_type" id="property_type">
                                    <option value="">-- Select --</option>
                                    <option value="HOUSE">HOUSE</option>
                                    <option value="APARTMENT">APARTMENT</option>
                                    <option value="FORM HOUSE">FORM HOUSE</option>
                                    <option value="FIELD">FIELD</option>
                                    <option value="OFFICE">OFFICE</option>
                                    <option value="PLOT">PLOT</option>
                                    <option value="OTHER">OTHER</option>
                                </select>
                            </div>
                            <div class="col-sm-3" style="margin-top: 28px;">
                                <button type="submit" class="btn btn-round btn-primary waves-effect">Search</button>
                                <button type="Reset" class="btn btn-round btn-primary waves-effect">Reset</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>           
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-hover table-striped table-bordered" id="example" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Seller</th>                                    
                                    <th data-breakpoints="xs">Buyer</th>
                                    <th data-breakpoints="xs sm md">Property Type</th>
                                    <th data-breakpoints="xs sm md">Status</th>
                                    <th data-breakpoints="xs">Action</th>
                                </tr>
                            </thead>
                                <tbody>

                                    <?php
                                     $data = "SELECT * FROM property WHERE page=2";
                                    $res = mysql_query($data,$conn);
                                    while ($row=mysql_fetch_assoc($res)) 
                                    { ?>
                                        <tr>
                                    <td>
                                        <span class="c_name"><?=$row['seller_id']?></span>
                                    </td>
                                    <td>
                                        <span class="phone"><?=$row['buyer_id']?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="email"><?=$row['property_type']?></span>
                                    </td>
                                    <td>
                                        <span class="email">
                                            <?php
                                                if($row['approve']=="N")
                                                {
                                                    echo "<span class='badge badge-danger'>Not Approved</span>";
                                                }
                                                else
                                                {
                                                    echo "<span class='badge badge-success'>Approved</span>";
                                                }
                                            ?>
                                        </span>
                                    </td>
                                    <td>
                                        <button onclick="javascript:location.href='approve_purchase.php?propertyid=<?=$row['propertyid']?>'" class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round"><i class="zmdi zmdi-edit"></i></button>
                                    </td>
                                </tr>
                                  <?php } ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Name</th>                                    
                                    <th data-breakpoints="xs">Phone</th>
                                    <th data-breakpoints="xs sm md">Email</th>
                                    <th data-breakpoints="xs sm md">Status</th>
                                    <th data-breakpoints="xs">Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
  
    </div>
</section>
<!-- Jquery Core Js -->

<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<?php include('includes/own.php'); ?>
</body>
</html>
<?php 
    if (isset($_POST['submit'])) 
    {
        $password = $_POST['password'];
        $userid = $_REQUEST['userid'];
        include "includes/config.php";
        $sql = "UPDATE user SET upwd='$password' WHERE userid='$userid' "; 
        $query = mysql_query($sql, $conn);
        if($query)
        {
            header('Location : index.php?reset_password=1')
        }
        else
        { 
            echo "<script>alert('Unexpected error found.');</script>";
        }
    }
?>


<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <?php include('includes/title.php'); ?>
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-purple authentication sidebar-collapse">
<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" style="background-image:url(assets/images/login.jpg)"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" method="post" action="index.php">
                    <div class="header">
                        <div class="logo-container">
                            <img src="assets/images/house.png" alt="">
                        </div>
                        <h5>Reset Password</h5>
                    </div>
                    <div class="content">                                                
                        <div class="input-group input-lg">
                            <input type="password" class="form-control" placeholder="Enter Password" name="password" autofocus autocomplete="off">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                        <div class="input-group input-lg">
                            <input type="password" placeholder="Confirm Password" class="form-control" name="con_password" autocomplete="off">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <!-- <a href="dashboard.php" class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light">SIGN IN</a> -->
                        <input type="submit" name="submit" class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light" value="Reset">
                        <!-- <h6 class="m-t-20"><a href="forgot_password.php" class="link">Forgot Password?</a></h6> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>
</body>
</html>
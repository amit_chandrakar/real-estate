﻿<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
    error_reporting(0);
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<?php include('includes/title.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
<style type="text/css">
    label
    {
        margin-left: 15px;
    }
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button 
    { 
        -webkit-appearance: none; 
    }
    #example2 tr th 
    {
        height: 10px;
    }
    #example3 tr th 
    {
        height: 10px;
    }
</style>
</head>
<body class="theme-purple" onload="aaa()">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-left">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-arrow-left"></i> Back</a></li>
                </ul>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Sales</li>
                </ul>                
            </div>
            <div class="col-lg-12 col-md-6 col-sm-12">
                <h2>Reports Information
                <small class="text-muted">Welcome to Compass</small>
                </h2>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">

                <div class="card action_bar">
                    <!-- <div class="header">
                        <h2><strong>Select Your</strong> Option <small>Description text here...</small> </h2>
                    </div> -->
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-3 col-6">
                                <label class="radio-inline"><input onchange="aaa()" id="op1" type="radio" name="optradio" <?php if (isset($_REQUEST['op']) && $_REQUEST['op']==1) {echo "checked";}?> > Rent Report</label>
                                <label class="radio-inline"><input onchange="aaa()" id="op2" type="radio" name="optradio" <?php if (isset($_REQUEST['op']) && $_REQUEST['op']==2) {echo "checked";}?>> Sale Report</label>
                                <label class="radio-inline"><input onchange="aaa()" id="op3" type="radio" name="optradio" <?php if (isset($_REQUEST['op']) && $_REQUEST['op']==3) {echo "checked";}?>> Purchase Report</label>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    function aaa()
                    {
                        if (document.getElementById("op1").checked == true) 
                        {
                            document.getElementById('sold_property').style.display ='none';
                            document.getElementById('purchase_property').style.display ='none';
                            document.getElementById('rent_property').style.display ='block';
                        }
                        else if(document.getElementById("op2").checked == true)
                        {
                            document.getElementById('purchase_property').style.display ='none';
                            document.getElementById('rent_property').style.display ='none';
                            document.getElementById('sold_property').style.display ='block';
                        }
                        else if(document.getElementById("op3").checked == true)
                        {
                            document.getElementById('rent_property').style.display ='none';
                            document.getElementById('sold_property').style.display ='none';
                            document.getElementById('purchase_property').style.display ='block';
                        }
                    }
                </script>



                <div class="card" id="rent_property">
                    <div class="header">
                        <h2><strong>Search Rent Property </strong> Information
                            <!-- <small>Description text here...</small> -->
                        </h2>
                    </div>
                    <div class="body">


                        <form>
                        <div class="row" style="margin-top: -25px;">
                           <input type="hidden" name="op" value="1">

                           <div class="col-sm-3">
                                <label>Rent Type</label>
                                <div class="form-group" style="margin-top: -20px;">
                                    <select class="form-control show-tick" name="rent_type" id="rent_type">
                                        <option value="">-- Select --</option>
                                        <option <?php if(isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN"){echo "selected";} ?> value="GIVEN">Given Property</option>
                                        <option <?php if(isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="TAKEN"){echo "selected";} ?> value="TAKEN">Taken Property</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-3">
                                <label>Owner</label>
                                <div class="form-group" style="margin-top: -20px;">
                                    <select class="form-control show-tick" name="owner1" id="owner1" >
                                        <option value="">-- Select --</option>
                                        <?php 
                                            $sql=mysql_query("SELECT * FROM profile",$conn);
                                            while ($res=mysql_fetch_assoc($sql)) 
                                            { ?>
                                                <option <?php if(isset($_REQUEST['owner1']) &&  $_REQUEST['owner1']==$res['userid']){echo "selected";} ?> value="<?=$res['userid']?>"><?=$res['fname']," ",$res['lname'] ?></option>
                                           <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label>Rent By</label>
                                <div class="form-group" style="margin-top: -20px;">
                                    <select class="form-control show-tick" name="rent_by1" id="rent_by1">
                                        <option value="">-- Select --</option>
                                        <?php 
                                            $sql=mysql_query("SELECT * FROM profile",$conn);
                                            while ($res=mysql_fetch_assoc($sql)) 
                                            { ?>
                                                <!-- echo "<option value=".$res['userid'].">".$res['fname']." ".$res['lname']."</option>"; -->

                                                <option <?php if(isset($_REQUEST['rent_by1']) &&  $_REQUEST['rent_by1']==$res['userid']){echo "selected";} ?> value="<?=$res['userid']?>"><?=$res['fname']," ",$res['lname'] ?></option>
                                          <?php  }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3" style="margin-top: 20px;">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-round btn-primary waves-effect">Search</button>
                                </div>
                            </div> 

                   
                       </div></form>
                           


                         <div class="row">
                           <div class="col-sm-12">
                            <?php
                                if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN") 
                                { ?>
                            <button id="btn-show-all-children" type="button">Expand All</button>
                            <button id="btn-hide-all-children" type="button">Collapse All</button>
                               <?php } ?>
                            
                            <table class="display table table-hover table-striped table-bordered table-condensed" id="example2">
                            <thead>
                                <tr>

                                <?php
                                if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN"){ ?>
                                <th data-breakpoints="xs">Seller Name</th>
                                <?php }
                                else
                                { ?>
                                    <th data-breakpoints="xs">Owner Name</th>
                              <?php } ?>

                                

                                <?php
                                if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN"){ ?>
                                <th data-breakpoints="xs">Owner Name</th>
                                <?php }
                                else
                                { ?>
                                    <th data-breakpoints="xs">Rent By </th>
                              <?php } ?>


                                <th data-breakpoints="xs sm md">Property Type</th>
                                <th data-breakpoints="xs">Location</th>
                                <th data-breakpoints="xs sm md">Area</th>

                                <?php
                                if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN"){ ?>
                                <th data-breakpoints="xs">Purchase Price</th>
                                <?php }
                                else
                                { ?>
                                    <th data-breakpoints="xs">Rent Amount</th>
                              <?php } ?>

                                <?php
                                if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN"){ ?>
                                <th class="none"></th>
                                <?php } ?>
                                </tr>
                            </thead>

                            <tfoot>
                                <tr>

                                <?php
                                if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN"){ ?>
                                <th data-breakpoints="xs">Seller Name</th>
                                <?php }
                                else
                                { ?>
                                    <th data-breakpoints="xs">Owner Name</th>
                              <?php } ?>

                                

                                <?php
                                if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN"){ ?>
                                <th data-breakpoints="xs">Owner Name</th>
                                <?php }
                                else
                                { ?>
                                    <th data-breakpoints="xs">Rent By </th>
                              <?php } ?>


                                <th data-breakpoints="xs sm md">Property Type</th>
                                <th data-breakpoints="xs">Location</th>
                                <th data-breakpoints="xs sm md">Area</th>

                                <?php
                                if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN"){ ?>
                                <th data-breakpoints="xs">Purchase Price</th>
                                <?php }
                                else
                                { ?>
                                    <th data-breakpoints="xs">Rent Amount</th>
                              <?php } ?>

                                <?php
                                if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN"){ ?>
                                <th class="none"></th>
                                <?php } ?>
                                </tr>
                            </tfoot>
                            

                            <tbody>
                                
<?php
if(isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']==''){$rent_type_cond="1=1"; }else if(isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=='GIVEN') {$rent_type_cond="pr.belong=1"." AND "."p.propertyid = pr.propertyid";}else if(isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=='TAKEN') {$rent_type_cond="pr.propertyid=0";}else{$rent_type_cond="0";}
if(isset($_REQUEST['owner1']) && $_REQUEST['owner1']==''){$owner1_cond="1=1"; } elseif(isset($_REQUEST['owner1']) && $_REQUEST['owner1']!='') {$owner1_cond="pr.owner='".$_REQUEST['owner1']."'";}else{$owner1_cond="0";}
if(isset($_REQUEST['rent_by1']) && $_REQUEST['rent_by1']==''){$rent_by1_cond="1=1"; }elseif(isset($_REQUEST['rent_by1']) && $_REQUEST['rent_by1']!='') {$rent_by1_cond="pr.rent_by=".$_REQUEST['rent_by1'];}else{$rent_by1_cond="0";}

    if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=='TAKEN') 
    {
        $com_dets = "SELECT * FROM property_rent pr WHERE ".$rent_type_cond." AND ".$owner1_cond." AND ".$rent_by1_cond." ";
    }
    else
    {
        $com_dets = "SELECT p.*, pr.* FROM property p, property_rent pr WHERE ".$rent_type_cond." AND ".$owner1_cond." AND ".$rent_by1_cond." GROUP BY pr.propertyid";
    }
    
    $rrs = mysql_query($com_dets, $conn);
                                        
        while ($res=mysql_fetch_assoc($rrs)) 
        { ?>
            <tr>
            <td>
                <?php
                    if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN")
                    {
                        get_name($res['seller_id']);
                    }
                    else
                    {
                        get_name($res['owner']);
                    }
                ?>        
            </td>
            <td>
                <?php
                    if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN")
                    {
                        get_name($res['buyer_id']);
                    }
                    else
                    {
                        get_name($res['rent_by']);
                    }
                ?> 
            </td>
            <td><?=$res['property_type']?></td>
            <td><?=$res['land_address']?></td>
            <td><?=$res['land_area']?></td>
            <td>
                <?php
                    if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN")
                    {
                        echo $res['receipt_amt'];
                    }
                    else
                    {
                        echo $res['rent_amtm'];
                    }
                ?>
            </td>
            <?php
            if (isset($_REQUEST['rent_type']) && $_REQUEST['rent_type']=="GIVEN") 
            { ?>
                <td>
                    <table width="100%" class="table-hover table-responsive table-bordered">
                        <tr>
                            <th>owner</th>
                            <th>rent_by</th>
                            <th>deposit_amt</th>
                            <th>rent_from</th>
                            <th>rent_to</th>
                            <th>Status</th>
                        </tr>
                        <tr>
                            <?php 
                                $rent = "SELECT * FROM property_rent pr INNER JOIN property p ON p.propertyid=pr.propertyid WHERE pr.propertyid='".$res['propertyid']."' ";
                                $rc = mysql_query($rent, $conn);
                                while($rt = mysql_fetch_assoc($rc))
                                { ?>
                                    <tr>
                                    <td><?=get_name($rt['owner']);?></td>
                                    <td><?=get_name($rt['rent_by']);?></td>
                                    <td><?=$rt['deposit_amt']?></td>
                                    <td><?=$rt['rent_from']?></td>
                                    <td><?=$rt['rent_to']?></td>
                                    <td><?php if($rt['closed']==1){echo "closed";}else{echo "Open";}?></td>
                                    </tr>
                              <?php  }
                            ?>
                        </tr>
                    </table>
                </td>
            <?php } ?>
            </tr>
      <?php  }
    ?>
                                
    </tbody>

      
                        </table>
                            </div> 
                        </div>
                    </div>
                </div>
















                <div class="card" id="sold_property" <?php if (isset($_REQUEST['op']) && $_REQUEST['op']==2){ echo "style='display: block;'";}else{echo "style='display: none;'";} ?> >
                    <div class="header">
                        <h2><strong>Search Sold Property </strong> Information
                            <!-- <small>Description text here...</small> -->
                        </h2>
                    </div>
                    <div class="body">
                        <form>
                        <div class="row" style="margin-top: -25px;">
                            <input type="hidden" name="op" value="2">
                            <div class="col-sm-3">
                                <label>Seller</label>
                                <div class="form-group" style="margin-top: -20px;">
                                    <select class="form-control show-tick" name="seller2" id="seller2" >
                                        <option value="">-- Select --</option>
                                        <?php 
                                            $sql=mysql_query("SELECT * FROM profile",$conn);
                                            while ($res=mysql_fetch_assoc($sql)) 
                                            { ?>
                                                <option <?php if(isset($_REQUEST['seller2']) &&  $_REQUEST['seller2']==$res['userid']){echo "selected";} ?> value="<?=$res['userid']?>"><?=$res['fname']," ",$res['lname'] ?></option>
                                          <?php  }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label>Buyer</label>
                                <div class="form-group" style="margin-top: -20px;">
                                    <select class="form-control show-tick" name="buyer2" id="buyer2">
                                        <option value="">-- Select --</option>
                                        <?php 
                                            $sql=mysql_query("SELECT * FROM profile",$conn);
                                            while ($res=mysql_fetch_assoc($sql)) 
                                            { ?>
                                                <option <?php if(isset($_REQUEST['buyer2']) &&  $_REQUEST['buyer2']==$res['userid']){echo "selected";} ?> value="<?=$res['userid']?>"><?=$res['fname']," ",$res['lname'] ?></option>
                                         <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label>Property Type</label>
                                <div class="form-group" style="margin-top: -20px;">
                                    <select class="form-control show-tick" name="property_type2" id="property_type2">
                                        <option value="">-- Select --</option>
                                        <option value="HOUSE">HOUSE</option>
                                        <option value="APARTMENT">APARTMENT</option>
                                        <option value="FORM HOUSE">FORM HOUSE</option>
                                        <option value="FIELD">FIELD</option>
                                        <option value="OFFICE">OFFICE</option>
                                        <option value="PLOT">PLOT</option>
                                        <option value="OTHER">OTHER</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3" style="margin-top: 20px;">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-round btn-primary waves-effect">Search</button>
                                </div>
                            </div> 

                            <div class="col-sm-3" >
                                <br>
                            </div>     
                        </div>
                        </form>





                            <div class="row">
                           <div class="col-sm-12">
                            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/r-2.2.1/datatables.min.css">
<button id="btn-show-all-children" type="button">Expand All</button>
<button id="btn-hide-all-children" type="button">Collapse All</button>
                            <table id="example3" class="display table-hover table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Seller Name</th>
                                        <th>Buyer Name</th>
                                        <th>Property Type</th>
                                        <th>Address</th>
                                        <th>Total Amount</th>
                                        <th class="none"></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Seller Name</th>
                                        <th>Buyer Name</th>
                                        <th>Property Type</th>
                                        <th>Address</th>
                                        <th>Total Amount</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php




if(isset($_REQUEST['seller2']) && $_REQUEST['seller2']==''){$seller2_cond="1=1"; }elseif(isset($_REQUEST['seller2']) && $_REQUEST['seller2']!=''){$seller2_cond="p.seller_id=".$_REQUEST['seller2'];}

if(isset($_REQUEST['buyer2']) && $_REQUEST['buyer2']==''){$buyer2_cond="1=1"; }elseif(isset($_REQUEST['buyer2']) && $_REQUEST['buyer2']!=''){$buyer2_cond="p.buyer_id=".$_REQUEST['buyer2'];}

if(isset($_REQUEST['property_type2']) && $_REQUEST['property_type2']==''){$property_cond="1=1"; }elseif(isset($_REQUEST['property_type2']) && $_REQUEST['property_type2']!=''){$property_cond="p.property_type="."'".$_REQUEST['property_type2']."'";}

if (isset($_REQUEST['seller2']) || isset($_REQUEST['buyer2']) || isset($_REQUEST['property_type2']) ) 
{
    $sql = "SELECT s.*, p.* FROM sale s, property p WHERE p.propertyid=s.propertyid AND ".$seller2_cond." AND ".$buyer2_cond." AND ".$property_cond." GROUP BY s.propertyid";
}
else
{

    $sql = "SELECT s.*, p.* FROM sale s, property p WHERE p.propertyid=s.propertyid GROUP BY s.propertyid";
}
                                        $query = mysql_query($sql , $conn);
                                        while($row = mysql_fetch_assoc($query))
                                        { ?>
                                            <tr>
                                            <td><?=get_name($row['seller_id'])?></td>
                                            <td><?=get_name($row['buyer_id'])?></td>
                                            <td><?=$row['property_type']?></td>
                                            <td><?=$row['land_address']?></td>
                                            <td><?=$row['total_amt']?></td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <th>Seller Name</th>
                                                        <th>Buyer Name</th>
                                                        <th>Property Type</th>
                                                        <th>Address</th>
                                                        <th>Sale Amount</th>
                                                    </tr>
                                                    
                                                        <?php
                                                            $sql1 = "SELECT * FROM sale where propertyid=".$row['propertyid'];
                                                            $query1= mysql_query($sql1 , $conn);
                                                            while($row1 = mysql_fetch_assoc($query1))
                                                            { ?>
                                                                <tr>
                                                                    <td><?=get_name($row1['seller_id'])?></td>
                                                                    <td><?=get_name($row1['buyer_id'])?></td>
                                                                <td><?=$row1['property_type']?></td>
                                                                <td><?=$row1['land_address']?></td>
                                                                <td><?=$row1['total_amt']?></td>
                                                                </tr>

                                                           <?php }
                                                        ?>
                                                </table>


                                            </td>
                                            </tr>
                                       <?php }


                                    ?>
                                </tbody>
                            </table>
                            </div> 
                        </div>
                    </div>
                </div>











                <div class="card" id="purchase_property" <?php if (isset($_REQUEST['op']) && $_REQUEST['op']==3){ echo "style='display: block;'";}else{echo "style='display: none;'";} ?>>
                    <div class="header">
                        <h2><strong>Search Purchased Property </strong> Information
                            <!-- <small>Description text here...</small> -->
                        </h2>
                    </div>
                    <div class="body">
                        <form>
                        <div class="row" style="margin-top: -25px;">
                            <input type="hidden" name="op" value="3">
                            <div class="col-sm-3">
                                <label>Seller</label>
                                <div class="form-group" style="margin-top: -20px;">
                                    <select class="form-control show-tick" name="seller3" id="seller3" >
                                        <option value="">-- Select --</option>
                                        <?php 
                                            $sql=mysql_query("SELECT * FROM profile",$conn);
                                            while ($res=mysql_fetch_assoc($sql)) 
                                            { ?>
                                                <option <?php if(isset($_REQUEST['seller3']) &&  $_REQUEST['seller3']==$res['userid']){echo "selected";} ?> value="<?=$res['userid']?>"><?=$res['fname']," ",$res['lname'] ?></option>
                                          <?php  }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label>Buyer</label>
                                <div class="form-group" style="margin-top: -20px;">
                                    <select class="form-control show-tick" name="buyer3" id="buyer3">
                                        <option value="">-- Select --</option>
                                        <?php 
                                            $sql=mysql_query("SELECT * FROM profile",$conn);
                                            while ($res=mysql_fetch_assoc($sql)) 
                                            { ?>
                                                <option <?php if(isset($_REQUEST['buyer3']) &&  $_REQUEST['buyer3']==$res['userid']){echo "selected";} ?> value="<?=$res['userid']?>"><?=$res['fname']," ",$res['lname'] ?></option>
                                          <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label>Property Type</label>
                                <div class="form-group" style="margin-top: -20px;">
                                    <select class="form-control show-tick" name="property_type3" id="property_type3">
                                        <option value="">-- Select --</option>
                                        <option value="HOUSE">HOUSE</option>
                                        <option value="APARTMENT">APARTMENT</option>
                                        <option value="FORM HOUSE">FORM HOUSE</option>
                                        <option value="FIELD">FIELD</option>
                                        <option value="OFFICE">OFFICE</option>
                                        <option value="PLOT">PLOT</option>
                                        <option value="OTHER">OTHER</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-3" style="margin-top: 20px;">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-round btn-primary waves-effect">Search</button>
                                </div>
                            </div>  
                        </div>
                        </form>





                            <div class="row">
                           <div class="col-sm-12">
                            <table class="table table-hover table-striped table-bordered table-condensed" id="example1" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="padding: 8px;" data-breakpoints="xs">Seller Name</th>
                                    <th style="padding: 8px;" data-breakpoints="xs">Buyer Name</th>
                                    <th style="padding: 8px;"  data-breakpoints="xs sm md">Property Type</th>
                                    <th style="padding: 8px;" data-breakpoints="xs">Location</th>
                                    <th style="padding: 8px;" data-breakpoints="xs sm md">Area</th>
                                    <th style="padding: 8px;" data-breakpoints="xs sm md">Amount</th>
                                </tr>
                            </thead>
                                <tbody>
                                    <?php

if(isset($_REQUEST['seller3']) && $_REQUEST['seller3']==''){$seller3_cond="1=1"; }elseif(isset($_REQUEST['seller3']) && $_REQUEST['seller3']!=''){$seller3_cond="seller_id=".$_REQUEST['seller3'];}

if(isset($_REQUEST['buyer3']) && $_REQUEST['buyer3']==''){$buyer2_cond="1=1"; }elseif(isset($_REQUEST['buyer3']) && $_REQUEST['buyer3']!=''){$buyer2_cond="buyer_id=".$_REQUEST['buyer3'];}

if(isset($_REQUEST['property_type3']) && $_REQUEST['property_type3']==''){$property3_cond="1=1"; }elseif(isset($_REQUEST['property_type3']) && $_REQUEST['property_type3']!=''){$property3_cond="property_type="."'".$_REQUEST['property_type3']."'";}

if (isset($_REQUEST['seller3']) || isset($_REQUEST['buyer3']) || isset($_REQUEST['property_type3']))
{
    $data = "SELECT * FROM property WHERE page=2 AND ".$seller3_cond."  AND ".$buyer2_cond."  AND ".$property3_cond." ";
}
else
{
    $data = "SELECT * FROM property WHERE page=2";
}



                                    $res = mysql_query($data,$conn);
                                    while ($row=mysql_fetch_assoc($res)) 
                                    { ?>
                                        <tr>
                                    <td>
                                        <span class="c_name"><?=get_name($row['seller_id'])?></span>
                                    </td>
                                    <td>
                                        <span class="c_name"><?=get_name($row['buyer_id'])?></span>
                                    </td>
                                    <td>
                                        <span class="email"><?=$row['property_type']?></span>
                                    </td>
                                    <td>
                                        <span class="phone"><?=$row['land_address']?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="email">
                                            <?php
                                                if ($row['property_type']=="APARTMENT") 
                                                {
                                                    echo "<b>1BHK : </b>".$row['1bhk'].", "."<b>2BHK : </b>".$row['2bhk'].", "."<b>3BHK : </b>".$row['3bhk'].", "."<b>4BHK : </b>".$row['4bhk'];
                                                }
                                                elseif($row['property_type']=="HOUSE")
                                                {
                                                    echo $row['p_amount'];
                                                    echo " BHK";
                                                }
                                                elseif($row['property_type']=="PLOT" || $row['property_type']=="FIELD" || $row['property_type']=="FORM HOUSE")
                                                {
                                                    echo $row['p_amount'];
                                                    echo " Acres";
                                                }
                                                elseif($row['property_type']=="OFFICE")
                                                {
                                                    echo $row['p_amount'];
                                                    echo " Rooms";
                                                }
                                            ?>
                                                
                                        </span>
                                    </td>
                                    <td>
                                        <span class="phone"><?=$row['total_amt']?>
                                        </span>
                                    </td>
                                   
                                </tr>
                                  <?php } ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="padding: 8px;" data-breakpoints="xs">Seller Name</th>
                                    <th style="padding: 8px;" data-breakpoints="xs">Buyer Name</th>
                                    <th style="padding: 8px;"  data-breakpoints="xs sm md">Property Type</th>
                                    <th style="padding: 8px;" data-breakpoints="xs">Location</th>
                                    <th style="padding: 8px;" data-breakpoints="xs sm md">Area</th>
                                    <th style="padding: 8px;" data-breakpoints="xs sm md">Amount</th>
                                </tr>
                            </tfoot>
                        </table>
                            </div> 
                        </div>
                    </div>
                </div>






















            
            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js --> 

<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script>


<!-- DataTable script -->
<!-- <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script> -->
<!-- DataTable script -->





<script type="text/javascript">
    $(document).ready(function() {
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
</script>
<!-- <script type="text/javascript">
    $(document).ready(function() {
    $('#example2').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
</script> -->
<!-- <script type="text/javascript">
    $(document).ready(function() {
    $('#example3').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
</script> -->
<?php include('includes/own.php'); ?>
</body>
</html>




<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/r-2.2.1/datatables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function (){
    var table = $('#example3').DataTable({
        'responsive': true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

    // Handle click on "Expand All" button
    $('#btn-show-all-children').on('click', function(){
        // Expand row details
        table.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
    });

    // Handle click on "Collapse All" button
    $('#btn-hide-all-children').on('click', function(){
        // Collapse row details
        table.rows('.parent').nodes().to$().find('td:first-child').trigger('click');
    });
});
</script>

<script type="text/javascript">
    $(document).ready(function (){
    var table = $('#example2').DataTable({
        'responsive': true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

    // Handle click on "Expand All" button
    $('#btn-show-all-children').on('click', function(){
        // Expand row details
        table.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
    });

    // Handle click on "Collapse All" button
    $('#btn-hide-all-children').on('click', function(){
        // Collapse row details
        table.rows('.parent').nodes().to$().find('td:first-child').trigger('click');
    });
});
</script>

